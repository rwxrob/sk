module gitlab.com/rwxrob/sk

go 1.14

require (
	github.com/ghodss/yaml v1.0.0
	gitlab.com/rwxrob/cmdtab v0.0.0-20200921025657-c517c1ac77b7
	gitlab.com/rwxrob/uniq v0.0.0-20200325203910-f771e6779384
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
